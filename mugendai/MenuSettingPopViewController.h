//
//  MenuSettingPopViewController.h
//  mugendai
//
//  Created by MINDNINE on 3/26/56 BE.
//  Copyright (c) 2556 mugendai. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol MenuSetting;
@interface MenuSettingPopViewController : UIViewController

- (IBAction)selectButton:(UIButton *)sender;
@property (nonatomic,strong) id<MenuSetting> delegate;
- (IBAction)closeMenuView:(id)sender;

@end

@protocol MenuSetting <NSObject>

-(void)menuSetting:(MenuSettingPopViewController *)vc didSelectMenuAtItem:(NSInteger)row;

@end