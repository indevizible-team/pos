//
//  GraphViewerViewController.h
//  POS
//
//  Created by Trash on 5/8/13.
//  Copyright (c) 2013 mugendai. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef enum {
    DMReportTypeDaily,
    DMReportTypeMonthly,
    DMReportTypeYearly,
    DMReportTypeAll
}DMReportType;

@interface GraphViewerViewController : UIViewController
@property (nonatomic,strong) NSString *year,*month,*date,*hour;
@property (nonatomic,assign) DMReportType reportType;
@end
