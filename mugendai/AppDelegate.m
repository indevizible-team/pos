//
//  AppDelegate.m
//  mugendai
//
//  Created by MINDNINE on 3/4/56 BE.
//  Copyright (c) 2556 mugendai. All rights reserved.
//

#import "AppDelegate.h"

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Override point for customization after application launch.
    defaultColor = UIColorFromRGB(6896663);
    [self customizeGlobalTheme];
    return YES;
}

- (void)customizeGlobalTheme {
    
    UIImage *navBarImage = [UIImage imageNamed:@"bgNavBar.png"];
    [[UINavigationBar appearance] setBackgroundImage:navBarImage forBarMetrics:UIBarMetricsDefault];
//    [[UIToolbar appearance] setTintColor:defaultColor];
//    [[UIDatePicker appearance] setTintColor:defaultColor];
    
    
    
    [[UIBarButtonItem appearance] setTintColor:[UIColor clearColor]];
    
    [[UINavigationBar appearance] setTitleTextAttributes: @{UITextAttributeTextColor: [UIColor whiteColor]
                                                        , UITextAttributeTextShadowColor:[UIColor grayColor]
                                                        , UITextAttributeTextShadowOffset: [NSValue valueWithUIOffset:UIOffsetMake(0, -1)]
                                                        , UITextAttributeFont: [UIFont fontWithName:@"Helvetica-Bold" size:18]}];
    
        
        UIImage *barButton = [[UIImage imageNamed:@"buttonDefault.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 15, 0, 5)];
        UIImage *barButtonx = [[UIImage imageNamed:@"buttonDefaultPress.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 15, 0, 5)];
        
        [[UIBarButtonItem appearance] setBackgroundImage:barButton forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
        [[UIBarButtonItem appearance] setBackButtonBackgroundImage:barButtonx forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
        
        
        [[UIBarButtonItem appearance] setTitleTextAttributes:
         [NSDictionary dictionaryWithObjectsAndKeys: [UIColor colorWithRed:112.0/255.0f green:112.0/255.0f blue:112.0/255.0f alpha:1.0f],
          UITextAttributeTextColor, [UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:1.0],
          UITextAttributeTextShadowColor, [NSValue valueWithUIOffset:UIOffsetMake(0, 0)],
          UITextAttributeTextShadowOffset, [UIFont fontWithName:@"Helvetica-Neue" size:14],
          UITextAttributeFont, nil] forState:UIControlStateNormal];
        
        [[UIBarButtonItem appearance] setTitleTextAttributes:
         [NSDictionary dictionaryWithObjectsAndKeys: [UIColor colorWithRed:255.0/255.0f green:102.0/255.0f blue:0.0/255.0f alpha:1.0f],
          UITextAttributeTextColor, [UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:1.0],
          UITextAttributeTextShadowColor, [NSValue valueWithUIOffset:UIOffsetMake(0, 0)],
          UITextAttributeTextShadowOffset, [UIFont fontWithName:@"Helvetica-Neue" size:15],
          UITextAttributeFont, nil] forState:UIControlStateHighlighted];
        
        
        [[UISegmentedControl appearance] setBackgroundImage:barButton forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
        [[UISegmentedControl appearance] setBackgroundImage:barButtonx forState:UIControlStateSelected barMetrics:UIBarMetricsDefault];
        [[UISegmentedControl appearance] setTintColor:[UIColor brownColor]];
        
        [[UISegmentedControl appearance] setTitleTextAttributes:
         [NSDictionary dictionaryWithObjectsAndKeys: [UIColor whiteColor],
          UITextAttributeTextColor, [UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:1.0],
          UITextAttributeTextShadowColor, [NSValue valueWithUIOffset:UIOffsetMake(0, 0)],
          UITextAttributeTextShadowOffset, [UIFont fontWithName:@"Helvetica-Neue" size:16],
          UITextAttributeFont, nil] forState:UIControlStateNormal];
        
        [[UISegmentedControl appearance] setTitleTextAttributes:
         [NSDictionary dictionaryWithObjectsAndKeys: [UIColor colorWithRed:255.0/255.0f green:102.0/255.0f blue:0.0/255.0f alpha:1.0f],
          UITextAttributeTextColor, [UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:1.0],
          UITextAttributeTextShadowColor, [NSValue valueWithUIOffset:UIOffsetMake(0, 0)],
          UITextAttributeTextShadowOffset, [UIFont fontWithName:@"Helvetica-Neue" size:16],
          UITextAttributeFont, nil] forState:UIControlStateSelected];


    
}
							
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
