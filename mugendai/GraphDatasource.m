//
//  GraphDatasource.m
//  POS
//
//  Created by Trash on 5/8/13.
//  Copyright (c) 2013 mugendai. All rights reserved.
//

#import "GraphDatasource.h"
#import "DMColor.h"
@implementation GraphDatasource

+(GraphDatasource *)dataSourceWithLabels:(NSArray *)labels graphType:(NSString*)graphType{
    GraphDatasource *ds = [GraphDatasource new];
    ds.dataSets = [NSMutableDictionary new];
    ds.dataSets[@"labels"] = labels;
    ds.graphType = graphType;
    ds.data = [NSMutableArray new];
    ds.dataSets[@"datasets"] = ds.data;
    return ds;
}
-(void)addDataSet:(NSArray *)dataSet withHUEColor:(float)color{
    [self.data addObject:[[DMColor colorFromHue:color] dictionaryColorWithGraphType:self.graphType data:dataSet]];
}
-(void)addDataSet:(NSArray *)dataSet withUIColor:(UIColor *)color{
    [self.data addObject:[[DMColor colorFromUIColor:color] dictionaryColorWithGraphType:self.graphType data:dataSet]];
}
@end
