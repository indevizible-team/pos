//
//  UICollectionView+DMScroll.h
//  Tester
//
//  Created by indevizible on 3/4/13.
//  Copyright (c) 2013 infostant. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UICollectionView (DMScroll)
-(void)scrollToHeaderIdentifier:(NSString *)identifier InSection:(NSUInteger)section animated:(BOOL)animated;
@end
