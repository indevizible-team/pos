//
//  OrderMenuViewController.m
//  mugendai
//
//  Created by MINDNINE on 3/4/56 BE.
//  Copyright (c) 2556 mugendai. All rights reserved.
//

#import "OrderMenuViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "ODRefreshControl.h"
#import "FoodHeader.h"
#import "OrderCell.h"
#import "DMNumPad.h"
#import "DataManager.h"
#import "MenuCollectionCell.h"
#import "MenuDetailsViewController.h"
#import "MenuSettingPopViewController.h"
#import "TestGraphViewController.h"
#import "DMURL.h"
@interface OrderMenuViewController ()<DMNumPadDelegate,DMNumPadDataSource,SendOrderDataSource,SendOrderDelegate,MenuSetting,UIAlertViewDelegate>{
    UIPopoverController *upc,*numpadPopOver;
     MenuDetailsViewController *menuDetail;
    DataController *dataSource;
    NSMutableArray *productData;
    NSMutableArray *orderData;
    Product *selectedProduct;
    NSUInteger selectedOrder;
    TestGraphViewController *testView;
    int totalQueue;
}
- (IBAction)changeWithNumpad:(UITextField *)sender;

@property (strong,nonatomic) UITextField *activeTextField;
@end

@implementation OrderMenuViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    UIImageView *backgroundImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"bg.png"]];
    
    [self.view addSubview:backgroundImage];
    [self.view sendSubviewToBack:backgroundImage];
    [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:NO];
    [super viewDidLoad];
    [self refreshUpdateButtonLabel];
    orderData = [NSMutableArray new];
    sectionList = @[];
	// Do any additional setup after loading the view.
    ODRefreshControl *refreshControl = [[ODRefreshControl alloc] initInScrollView:self.tableorder];
    [refreshControl setTintColor:[UIColor brownColor]];
    [refreshControl setActivityIndicatorViewColor:[UIColor whiteColor]];
    [refreshControl addTarget:self action:@selector(dropViewDidBeginRefreshing:) forControlEvents:UIControlEventValueChanged];
    NSMutableDictionary *product = [PLIST readFeature:@"product"];
    if ([product count]) {
        dataSource = [[DataController alloc] initWithDictionary:product];
        NSMutableArray *keyList = [NSMutableArray new];
        for (NSString *key in dataSource.key) {
            [keyList addObject:dataSource.data[key][@"catname"]];
        }
        sectionList = keyList;
        productData = [NSMutableArray new];
        for (NSString *key in dataSource.key) {
            [productData addObject:[[DataController alloc] initWithDictionary:dataSource.data[key][@"product"]]];
        }
        [self.collectionMenu reloadData];
    }
    NSMutableDictionary *parameters = [DataManager param];
    [parameters setValue:@2 forKey:@"infofid"];
	NSMutableURLRequest *myRequest = [[DataManager client] requestWithMethod:@"GET" path:@"getNewAllFeaturedata" parameters:parameters];
	AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:myRequest success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
        NSLog(@"%@",request.URL);
		if ([JSON count]) {
            dataSource = [[DataController alloc] initWithDictionary:[PLIST writeFeature:@"product" withData:JSON]];
            NSMutableArray *keyList = [NSMutableArray new];
            for (NSString *key in dataSource.key) {
                [keyList addObject:dataSource.data[key][@"catname"]];
            }
            sectionList = keyList;
            productData = [NSMutableArray new];
            for (NSString *key in dataSource.key) {
                [productData addObject:[[DataController alloc] initWithDictionary:dataSource.data[key][@"product"]]];
            }
            [self.collectionMenu reloadData];
            
        }
	} failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
        
	}];
	[operation start];
    
}
- (void)dropViewDidBeginRefreshing:(ODRefreshControl *)refreshControl
{
    double delayInSeconds = 3.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [refreshControl endRefreshing];
    });
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    if (dataSource !=nil) {
        return dataSource.key.count ;
    }
    return 0;
}


- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section
{
    if (productData != nil) {
        DataController *tc  = productData[section];
        return tc.key.count;
    }
    return 0;
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    FoodHeader *catBanner = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader                                                       withReuseIdentifier:@"foodtype" forIndexPath:indexPath];
    [catBanner.foodTypeName setText:sectionList[indexPath.section]];
    return catBanner;

}
//-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section
- (UICollectionViewCell *)collectionView:(UICollectionView *)cv cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    DataController *thisCategory = productData[indexPath.section];
    NSMutableDictionary *thisProduct = [thisCategory dictionaryAtIndex:indexPath.row];
    MenuCollectionCell *cell;
    cell = [cv dequeueReusableCellWithReuseIdentifier:@"menuCell" forIndexPath:indexPath];
    [cell.menuImage setImageWithURL:[NSURL URLWithString:thisProduct[@"pic"]]];
    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    DataController *thisProduct = productData[indexPath.section];
   
    selectedProduct = [Product productWithData:[thisProduct dictionaryAtIndex:indexPath.row]];
    NSLog(@"%@",[thisProduct dictionaryAtIndex:indexPath.row]);
    menuDetail = (MenuDetailsViewController *)[DataManager controllerByID:@"MenuDetail"];
    menuDetail.delegate = self;
    [self presentViewController:menuDetail animated:NO completion:nil];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (orderData) {
        return orderData.count;
    }
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)aTableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    OrderCell *cell = [aTableView dequeueReusableCellWithIdentifier:@"OrderCell" forIndexPath:indexPath];
    cell.btnCancel.tag = indexPath.row;
    Order *eachOrder = orderData[indexPath.row];
    cell.orderName.text = eachOrder.product.name;
    cell.orderSet.text = [NSString stringWithFormat:@"%i",eachOrder.quantity];
    [cell.orderImg setImageWithURL:eachOrder.product.imageURL];
    return cell;
}

- (void)tableView:(UITableView *)aTableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [aTableView deselectRowAtIndexPath:indexPath animated:YES];
    Order *eachOrder = orderData[indexPath.row];
    selectedProduct = eachOrder.product;
    menuDetail = (MenuDetailsViewController *)[DataManager controllerByID:@"MenuDetail"];
    menuDetail.delegate = self;
    [self presentViewController:menuDetail animated:NO completion:nil];
   
}
- (IBAction)deleteItem:(UIButton *)sender {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert!" message:@"Do you want to remove this order?" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Yes", nil];
    alert.tag = sender.tag;
    [alert show];
    
}
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex) {
        [orderData removeObjectAtIndex:alertView.tag];
        [self.tableorder reloadData];
        [self reloadPriceLabel];
    }
}


- (IBAction)popSettingMenu:(id)sender {
    UIStoryboard *storyboard;
    NSDictionary *storyboardList = [[NSBundle mainBundle] infoDictionary];
    storyboard = [UIStoryboard storyboardWithName:[storyboardList valueForKey:@"UIMainStoryboardFile"] bundle:nil];
    
    MenuSettingPopViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"MenuSetting"];
//    vc.delegate = self;
    [self presentViewController:vc animated:NO completion:nil];
}

- (IBAction)selectFoodType:(UIButton *)sender {
    UIStoryboard *storyboard;
    NSDictionary *storyboardList = [[NSBundle mainBundle] infoDictionary];
    storyboard = [UIStoryboard storyboardWithName:[storyboardList valueForKey:@"UIMainStoryboardFile"] bundle:nil];
    
    FoodSelectViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"FoodView"];
    vc.delegate = self;
    upc = [[UIPopoverController alloc] initWithContentViewController:vc];
    upc.delegate= self;
    
    [upc setPopoverContentSize:CGSizeMake(200, 200) animated:YES];
    [upc presentPopoverFromRect:sender.frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
}

-(void)foodSelect:(FoodSelectViewController *)vc atSection:(NSUInteger)section{
    [upc dismissPopoverAnimated:YES];
//    [self.collectionMenu scrollToHeaderIdentifier:@"menuCell" InSection:section animated:YES];
}

-(NSArray *)sectionDataFor:(FoodSelectViewController *)vc{
    return sectionList;
}
- (IBAction)changeWithNumpad:(UITextField *)sender {
    self.activeTextField = sender;
    [sender resignFirstResponder];
    UIStoryboard *storyboard;
    NSDictionary *storyboardList = [[NSBundle mainBundle] infoDictionary];
    storyboard = [UIStoryboard storyboardWithName:[storyboardList valueForKey:@"UIMainStoryboardFile"] bundle:nil];
    
    DMNumPad *vc = [storyboard instantiateViewControllerWithIdentifier:@"DMNumPad"];
    vc.delegate = self;
    numpadPopOver.delegate = self;
    numpadPopOver = [[UIPopoverController alloc] initWithContentViewController:vc];
    [numpadPopOver setPopoverContentSize:CGSizeMake(195, 185) animated:YES];
    [numpadPopOver presentPopoverFromRect:sender.frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
    
}

-(void)numpadDidChangeValue:(DMNumPad *)vc value:(NSString *)value{
    if (self.activeTextField.tag == 99) {
        [self.changemoney setText:[NSString stringWithFormat:@"%.0f",[value integerValue]-[self getSum]]];
    }
    [self.activeTextField setText:value];
}
-(void)numpadDidClose:(DMNumPad *)vc{
    [numpadPopOver dismissPopoverAnimated:YES];
}
-(NSString *)numpadTargetString:(DMNumPad *)vc{
    return self.activeTextField.text;
}
- (IBAction)actionClear:(id)sender {
    [self clearOrder];
}

-(NSMutableArray *)orderListProduct:(MenuDetailsViewController *)vc{
    return orderData;
}
-(Product *)orderProduct:(MenuDetailsViewController *)vc{
    return selectedProduct;
}
-(void)orderProduct:(MenuDetailsViewController *)vc saveOrder:(Order *)order{
    [self.tableorder reloadData];
    if (orderData.count >0) {
        [self.tableorder scrollToRowAtIndexPath:[NSIndexPath indexPathForItem:orderData.count-1 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
    }
    [self reloadPriceLabel];
}
-(void)reloadPriceLabel{
    [self.totalLabel setText:[NSString stringWithFormat:@"Total: %.0f Baht",[self getSum]]];
    [self.changemoney setText:[NSString stringWithFormat:@"%.0f",[self.recievemoney.text integerValue]-[self getSum]]];
}
-(float)getSum{
    float sum = 0.0f;
    for (Order *od in orderData) {
        sum += od.product.salePrice * od.quantity;
    }
    return sum;
}
- (IBAction)completeOrder:(UIButton *)sender {
    NSMutableDictionary *stat = [PLIST readFeature:@"stat"];
    NSString *hour,*date,*month,*year;
    NSMutableDictionary *currentHour,*currentDate,*currentMonth,*currentYear,*currentAll;
    hour = [DataManager currentHour];
    date = [DataManager currentDate];
    month = [DataManager currentMonth];
    year = [DataManager currentYear];
    
    if (!stat[year]) {
        stat[year] =[NSMutableDictionary new];
    }
    if (!stat[year][month]) {
        stat[year][month] =[NSMutableDictionary new];
    }
    if (!stat[year][month][date]) {
        stat[year][month][date] =[NSMutableDictionary new];
    }
    if (!stat[year][month][date][hour]) {
        stat[year][month][date][hour] =[NSMutableDictionary new];
    }
    currentHour = stat[year][month][date][hour];
    currentDate = stat[year][month][date];
    currentMonth  =stat[year][month];
    currentYear = stat[year];
    currentAll = stat;
    NSArray *current = @[currentHour,currentDate,currentMonth,currentYear,currentAll];
    if (orderData.count) {
        NSMutableDictionary *data ;
        data = [DataManager param];
        [data setValue:self.memberid.text forKey:@"name"];
        [data setValue:@0 forKey:@"table"];
        [data setValue:@0 forKey:@"discount"];
        
        NSMutableDictionary *list = [NSMutableDictionary new];
        for (Order *eachOrder in orderData) {
            NSMutableDictionary* tmpData = [NSMutableDictionary new];
            [tmpData setValue:eachOrder.product.id forKey:@"productid"];
            [tmpData setValue:[NSNumber numberWithInt:eachOrder.quantity] forKey:@"num"];
            [tmpData setValue:eachOrder.remark forKey:@"remark"];
            [list setValue:tmpData forKey:[NSString stringWithFormat:@"%@",eachOrder.product.id]];
            
            NSString *key = [NSString stringWithFormat:@"%@-%@",eachOrder.product.catid,[eachOrder.product.id stringValue]];
            for (NSMutableDictionary  *object in current) {
                if (!object[@"data"]) {
                    object[@"data"] = [NSMutableDictionary new];
                }
                
                if (object[@"data"][key]) {
                    NSNumber *oldValue = object[@"data"][key];
                    object[@"data"][key] = [NSNumber numberWithInt:eachOrder.quantity+[oldValue intValue]];
                }else{
                    object[@"data"][key] = [NSNumber numberWithInt:eachOrder.quantity];
                }
                NSLog(@"%@",object);
                
            }
            
            
                        
        }
         
        [data setValue:list forKey:@"data"];
        
        NSMutableDictionary *orderQueue = [PLIST readFeature:@"orderQueue"];
        NSMutableArray *saveDataItem = [[NSMutableArray alloc] initWithObjects:data, nil];
        if ([orderQueue valueForKey:@"data"]==nil) {
            [orderQueue setValue:saveDataItem forKey:@"data"];
        }else{
            [[orderQueue valueForKey:@"data"] addObject:data];
        }
        
      
        
        data = [PLIST writeFeature:@"orderQueue" withData:orderQueue];
        [self refreshUpdateButtonLabel];
        [self clearOrder];
    }
    [PLIST writeFeature:@"stat" withData:stat];
}
-(void)refreshUpdateButtonLabel{
    NSMutableDictionary *data = [PLIST readFeature:@"orderQueue"];
    int count = [data[@"data"] count];
    [self.updateBtn setTitle:((count)?[NSString stringWithFormat:@"Update (%i)",count]:@"Update") forState:UIControlStateNormal];
}
-(void)clearOrder{
    self.couponid.text = self.memberid.text = self.recievemoney.text = nil;
    self.changemoney.text = @"0";
    orderData = [NSMutableArray new];
    [self.tableorder reloadData];
}
- (IBAction)updateAction:(UIButton *)sender {

    NSMutableDictionary *order = [PLIST readFeature:@"orderQueue"];
    NSMutableArray *eachOrderData = order[@"data"];
    if (eachOrderData) {
        if (eachOrderData.count) {
            if (sender) {
                totalQueue = eachOrderData.count;
            }
            NSMutableDictionary *param = eachOrderData[0];
            NSLog(@"param : %@ ",param  );
            AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:[[DataManager client] requestWithMethod:@"POST" path:@"saveorderproductdata" parameters:param] success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
                [eachOrderData removeObject:param];
                NSMutableDictionary *writenData = [PLIST writeFeature:@"orderQueue" withData:@{@"data":eachOrderData }];
                [self refreshUpdateButtonLabel];
                if ([writenData[@"data"] count]) {
                    [self updateAction:nil];
                }else{
                    totalQueue = 0;
                    [SVProgressHUD showSuccessWithStatus:@"Send all order completed"];
                }
                
            } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Can not conect to server please try again" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                [alert show];
            }];
            [operation setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
                float progress = totalBytesWritten / (float)totalBytesExpectedToWrite;
                float percentPerOrder = 1.0/(totalQueue);
                int left = eachOrderData.count;
                int finish = totalQueue - left;
                float showProgress = ((finish-1)*percentPerOrder)+(progress/percentPerOrder);
                [SVProgressHUD showProgress:showProgress];
            }];
            [operation start];
        }
    }
  
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([segue.identifier isEqual:@"setting"]) {
        MenuSettingPopViewController *vc = [segue destinationViewController];
        vc.delegate = self;
    }
}

-(void)menuSetting:(MenuSettingPopViewController *)vc didSelectMenuAtItem:(NSInteger)row{
    if (row==2) {
        [vc dismissViewControllerAnimated:NO completion:^{
//            id theVC = [DataManager controllerByID:@"statView"];
//            [self.navigationController pushViewController:theVC animated:YES];
            [self performSegueWithIdentifier:@"s5078f" sender:self];
        }];
    }
}
@end
