//
//  MenuSettingPopViewController.m
//  mugendai
//
//  Created by MINDNINE on 3/26/56 BE.
//  Copyright (c) 2556 mugendai. All rights reserved.
//

#import "MenuSettingPopViewController.h"
#import "OrderHistoryViewController.h"
#import "UserManagerViewController.h"
#import "UserManageProfileViewController.h"
#import "RouteListViewController.h"
#import "ProfileSettingViewController.h"

@interface MenuSettingPopViewController ()

@end

@implementation MenuSettingPopViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (IBAction)selectButton:(UIButton *)sender {
    if ([self.delegate respondsToSelector:@selector(menuSetting:didSelectMenuAtItem:)]) {
        [self.delegate menuSetting:self didSelectMenuAtItem:sender.tag-1];
    }
    UIStoryboard *storyboard;
    NSDictionary *storyboardList = [[NSBundle mainBundle] infoDictionary];
    storyboard = [UIStoryboard storyboardWithName:[storyboardList valueForKey:@"UIMainStoryboardFile"] bundle:nil];
    
    if (sender.tag==1) {
        OrderHistoryViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"OrderHistory"];
        [self presentViewController:vc animated:NO completion:nil];
    }
    if (sender.tag==2) {
        UserManagerViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"UserManager"];
        [self presentViewController:vc animated:NO completion:nil];
        
    }
    else if (sender.tag==3) {
        
    }
    else if (sender.tag==4) {
        RouteListViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"RouteView"];
        [self presentViewController:vc animated:NO completion:nil];
        
    }
    else if (sender.tag==5) {
        
    }
    else if (sender.tag==6) {
        
    }
    else if (sender.tag==7) {
        
    }
    else if (sender.tag==8) {
        
    }
    else if (sender.tag==9) {
        ProfileSettingViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"ProfileSetting"];
        [self presentViewController:vc animated:NO completion:nil];
        
    }
}

- (IBAction)closeMenuView:(id)sender {
    [self dismissViewControllerAnimated:NO completion:nil];
}
@end
