//
//  DataController.m
//  infostant
//
//  Created by Nattawut Singhchai on 12/29/12 .
//  Copyright (c) 2012 infostant. All rights reserved.
//

#import "DataController.h"
#import "DataManager.h"
@implementation DataController
@synthesize controller,data;
-(id)init{
	self= [super init];
	if (self) {
		controller = [NSMutableArray new];
	}
	return self;
}
-(id)initWithDictionary:(NSMutableDictionary *)dictionary{
    NSMutableDictionary *tmpDic =[@{@"key": @[]} mutableCopy];
	self = [super init];
    if([dictionary count]){
        tmpDic = dictionary;
    }
    
	if (self) {
		if([dictionary valueForKey:@"data"]==nil){
			data = tmpDic;
		}else{
			data = [tmpDic valueForKey:@"data"];
		}
		
		controller = [[NSMutableArray alloc] init];
		[controller addObject:self];
	}
	return self;
}

-(NSArray *)key{

		if ([data valueForKey:@"key"]!=nil) {
			return [data valueForKey:@"key"];
		}
	
	return nil;
}
-(DataController *)accessKey:(id)key{

		if ([data valueForKey:key]!=nil) {
			
			DataController *_tmpDataController = [[DataController alloc]init];
            _tmpDataController.name = key;
            
            NSMutableDictionary *_tmpData = [[data valueForKey:key] valueForKey:@"data"];
			_tmpDataController.controller = [[NSMutableArray alloc] initWithArray:self.controller copyItems:NO];
           
			
			
			if (_tmpData == nil) {
				_tmpDataController.data = [@{} mutableCopy];
			}else{
				_tmpDataController.data = _tmpData;
			}
			
			[_tmpDataController.controller addObject:_tmpDataController];
			return _tmpDataController;
		}
	
	return nil;
}
-(DataController *)parent{
	if ([controller count]>1) {
		DataController *_tmpDataController = [DataController new];
		_tmpDataController.controller = [[NSMutableArray alloc] initWithArray:self.controller copyItems:NO];
		[_tmpDataController.controller removeLastObject];
		_tmpDataController = [_tmpDataController.controller lastObject];
		return _tmpDataController;
	}
	return nil;
}




-(DataController*)insertData:(NSMutableDictionary *)dataToInsert{
    if (self==nil) {
        return [[DataController alloc] initWithDictionary:dataToInsert];
    }
	if ([dataToInsert valueForKey:@"key"]!=nil) {
		if ([self.key count]) {
			NSMutableArray *tmpKey = [dataToInsert valueForKey:@"key"];
            [[data valueForKey:@"key"] addObjectsFromArray:[tmpKey mutableCopy]];
            
            [dataToInsert removeObjectForKey:@"key"];
            [data setValuesForKeysWithDictionary:dataToInsert];
			return self;
		}else{
			data = [DataManager JSONToMutableDictionary:dataToInsert];
		}
		return self;
	}else if ([dataToInsert valueForKey:@"data"] != nil) {
		NSMutableDictionary *dataToMerge = [dataToInsert valueForKey:@"data"];
		NSArray *_key = [dataToMerge valueForKey:@"key"];
        if ([self.key count]) {
            [[data valueForKey:@"key"] addObjectsFromArray:_key];
            [dataToMerge removeObjectForKey:@"key"];
            [data setValuesForKeysWithDictionary:dataToMerge];
			return self;
		}else{
			data = [DataManager JSONToMutableDictionary:dataToMerge];
		}
	}
	return nil;
}
-(DataController*)insertDataToFront:(NSMutableDictionary *)dataToInsert{
	if ([dataToInsert valueForKey:@"data"]!=nil) {
		NSMutableDictionary *dataToMerge = [dataToInsert valueForKey:@"data"];
		NSMutableArray *_key = [[dataToMerge valueForKey:@"key"] mutableCopy];
		if (self.key.count) {
            NSMutableArray *_oldData = [[data valueForKey:@"key"] mutableCopy];
            [_key addObjectsFromArray:_oldData];
            [[data valueForKey:@"key"] addObjectsFromArray:_key];
            [data setValue:_key forKey:@"key"];
            
            [dataToMerge removeObjectForKey:@"key"];
            [data setValuesForKeysWithDictionary:dataToMerge];
			return self;
		}else{
			data = [DataManager JSONToMutableDictionary:dataToMerge];
		}
		return self;
	}else if ([dataToInsert valueForKey:@"key"] != nil) {
		if (data.count) {
            NSMutableArray *_key = [[dataToInsert valueForKey:@"key"] mutableCopy];
            NSMutableArray *_oldData = [[data valueForKey:@"key"] mutableCopy];
            [_key addObjectsFromArray:_oldData];
            [[data valueForKey:@"key"] addObjectsFromArray:_key];
            [data setValue:_key forKey:@"key"];
            
            [dataToInsert removeObjectForKey:@"key"];
            [data setValuesForKeysWithDictionary:dataToInsert];
			return self;
		}else{
			data = [DataManager JSONToMutableDictionary:dataToInsert];
		}
		
	}
	return nil;
}

-(NSMutableDictionary *)dictionaryAtIndex:(NSUInteger)index{
    return [self.data valueForKey:self.key[index]];
}
@end
