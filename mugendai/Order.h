//
//  Order.h
//  mugendai
//
//  Created by indevizible on 3/5/13.
//  Copyright (c) 2013 mugendai. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Product.h"
@interface Order : NSObject
@property (nonatomic,strong) Product *product;
@property (nonatomic,assign) int quantity;
@property (nonatomic,strong) NSNumber *productid;
@property (nonatomic,strong) NSString *remark;

+(Order *)orderFromProduct:(Product *)prd quantity:(int)quantity;
-(NSDictionary*)getOrderParm;
@end
