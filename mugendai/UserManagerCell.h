//
//  UserManagerCell.h
//  mugendai
//
//  Created by MINDNINE on 3/26/56 BE.
//  Copyright (c) 2556 mugendai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UserManagerCell : UICollectionViewCell

@property (strong, nonatomic) IBOutlet UIImageView *imageProfile;
@property (strong, nonatomic) IBOutlet UILabel *username;
@property (strong, nonatomic) IBOutlet UILabel *placeName;
@property (strong, nonatomic) IBOutlet UILabel *weeklyNumber;
@property (strong, nonatomic) IBOutlet UILabel *monthlyNumber;
@property (strong, nonatomic) IBOutlet UILabel *totalNumber;
@end
