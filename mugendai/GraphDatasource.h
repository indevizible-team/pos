//
//  GraphDatasource.h
//  POS
//
//  Created by Trash on 5/8/13.
//  Copyright (c) 2013 mugendai. All rights reserved.
//
#import "DMGraphDefine.h"
#import <Foundation/Foundation.h>

@interface GraphDatasource : NSObject
@property (strong, nonatomic) NSMutableDictionary *dataSets;
@property (strong,nonatomic) NSString *graphType;
@property (strong,nonatomic) NSMutableArray *data;
+(GraphDatasource *)dataSourceWithLabels:(NSArray *)labels graphType:(NSString*)graphType;
-(void)addDataSet:(NSArray *)dataSet withHUEColor:(float)color;
-(void)addDataSet:(NSArray *)dataSet withUIColor:(UIColor *)color;
@end
