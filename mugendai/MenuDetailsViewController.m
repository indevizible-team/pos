//
//  MenuDetailsViewController.m
//  mugendai
//
//  Created by MINDNINE on 3/4/56 BE.
//  Copyright (c) 2556 mugendai. All rights reserved.
//

#import "MenuDetailsViewController.h"
#import "DataManager.h"
@interface MenuDetailsViewController (){
    Product *productToOrder;
    NSMutableArray *orderList;
    Order *eachOrder;
    int index;
    BOOL isNewOrder;
}
@property (strong, nonatomic) IBOutlet UIStepper *stepper;
@property (strong, nonatomic) IBOutlet UIImageView *menuImage;
@property (strong, nonatomic) IBOutlet UILabel *menuName;
@property (strong, nonatomic) IBOutlet UITextView *menuInfo;
@property (strong, nonatomic) IBOutlet UILabel *price;
@property (strong, nonatomic) IBOutlet UITextField *quantity;
@property (strong, nonatomic) IBOutlet UIButton *sendOrder;

@end

@implementation MenuDetailsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    isNewOrder = NO;
	// Do any additional setup after loading the view.
    if ([self.delegate respondsToSelector:@selector(orderProduct:)]) {
        productToOrder = [self.delegate orderProduct:self];
    }
    if ([self.delegate respondsToSelector:@selector(orderListProduct:)]) {
        orderList = [self.delegate orderListProduct:self];
    }
    
    for (Order *order in orderList) {
        if ([order.productid isEqualToNumber:productToOrder.id]) {
            eachOrder = order;
            break;
        }
    }
    if (!eachOrder) {
        [orderList addObject:[Order orderFromProduct:productToOrder quantity:0]];
        eachOrder = [orderList lastObject];
        isNewOrder = YES;
    }
    if (!eachOrder.quantity) {
        self.stepper.value = 1.0f;
    }else{
        self.stepper.value = eachOrder.quantity;
    }
    
    self.quantity.text = [NSString stringWithFormat:@"%i",(int)self.stepper.value];
    [self.menuImage setImageWithURL:productToOrder.imageURL];
    self.menuName.text = productToOrder.name;
    self.menuInfo.text = productToOrder.description;
    self.price.text = [NSString stringWithFormat:@"%.0f Baht",productToOrder.salePrice];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)sendOrder:(id)sender {
    
    eachOrder.quantity = (int)self.stepper.value;
    if ([self.delegate respondsToSelector:@selector(orderProduct:saveOrder:)]) {
        if (!eachOrder.quantity) {
            [orderList removeObject:eachOrder];
        }
            
        
        [self.delegate orderProduct:self saveOrder:eachOrder];
    }
    [self dismissViewControllerAnimated:NO completion:nil];
}
- (IBAction)closeDetails:(id)sender {
    if (isNewOrder) {
        [orderList removeLastObject];
    }
    [self dismissViewControllerAnimated:NO completion:nil];
}
- (IBAction)stepping:(UIStepper *)sender {
    self.quantity.text  = [NSString stringWithFormat:@"%.0f",sender.value];
}


@end
