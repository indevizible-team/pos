//
//  FoodHeader.h
//  mugendai
//
//  Created by indevizible on 3/4/13.
//  Copyright (c) 2013 mugendai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FoodHeader : UICollectionReusableView
@property (strong, nonatomic) IBOutlet UILabel *foodTypeName;

@end
