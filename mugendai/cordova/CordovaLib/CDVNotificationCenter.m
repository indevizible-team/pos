//
//  CDVNotificationCenter.m
//  POS
//
//  Created by Trash on 5/15/13.
//  Copyright (c) 2013 mugendai. All rights reserved.
//

#import "CDVNotificationCenter.h"
#import "CDV.h"
@implementation CDVNotificationCenter
- (void)postNotification:(CDVInvokedUrlCommand*)command
{
    id message = [command.arguments objectAtIndex:0];
    
    NSLog(@"postNotification : %@",message);
    [[NSNotificationCenter defaultCenter] postNotification:[NSNotification notificationWithName:CDVLocalNotification object:message]];
    
//    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:message];
//    
//    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}
@end
