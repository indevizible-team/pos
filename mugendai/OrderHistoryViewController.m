//
//  OrderHistoryViewController.m
//  mugendai
//
//  Created by MINDNINE on 3/26/56 BE.
//  Copyright (c) 2556 mugendai. All rights reserved.
//

#import "OrderHistoryViewController.h"
#import "OrderHistoryCell.h"
#import "RouteListViewController.h"
@interface OrderHistoryViewController ()

@end

@implementation OrderHistoryViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return 6;
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)cv cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    OrderHistoryCell *cell;
    cell = [cv dequeueReusableCellWithReuseIdentifier:@"OrderHistoryCell" forIndexPath:indexPath];
    return cell;
}
- (IBAction)backButton:(id)sender {
    [self dismissViewControllerAnimated:NO completion:nil];
}

- (IBAction)routeViewOrder:(id)sender {
    UIStoryboard *storyboard;
    NSDictionary *storyboardList = [[NSBundle mainBundle] infoDictionary];
    storyboard = [UIStoryboard storyboardWithName:[storyboardList valueForKey:@"UIMainStoryboardFile"] bundle:nil];
    
    RouteListViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"RouteView"];
    [self presentViewController:vc animated:NO completion:nil];
}

@end
