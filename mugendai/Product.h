//
//  Product.h
//  mugendai
//
//  Created by indevizible on 3/5/13.
//  Copyright (c) 2013 mugendai. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Product : NSObject
@property (nonatomic,strong) NSNumber *id;
@property (nonatomic,strong) NSString *catid;
@property (nonatomic,strong) NSString *name;
@property (nonatomic,strong) NSString *description;
@property (nonatomic,assign) float regularPrice;
@property (nonatomic,assign) float salePrice;
@property (nonatomic,strong) NSURL *imageURL;
+(Product *)productWithData:(NSDictionary *)data;
@end
