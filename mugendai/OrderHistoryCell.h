//
//  OrderHistoryCell.h
//  mugendai
//
//  Created by MINDNINE on 3/26/56 BE.
//  Copyright (c) 2556 mugendai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OrderHistoryCell : UICollectionViewCell

@property (strong, nonatomic) IBOutlet UILabel *orderNumber;
@property (strong, nonatomic) IBOutlet UILabel *userName;
@property (strong, nonatomic) IBOutlet UILabel *time;
@property (strong, nonatomic) IBOutlet UILabel *route;
@property (strong, nonatomic) IBOutlet UIButton *deleteOrder;
@property (strong, nonatomic) IBOutlet UIButton *searchButton;
@property (strong, nonatomic) IBOutlet UIButton *colorLabel;
@end
