//
//  UICollectionView+DMScroll.m
//  Tester
//
//  Created by indevizible on 3/4/13.
//  Copyright (c) 2013 infostant. All rights reserved.
//

#import "UICollectionView+DMScroll.h"
#import "DMFlowLayout.h"
@implementation UICollectionView (DMScroll)
-(void)scrollToHeaderIdentifier:(NSString *)identifier InSection:(NSUInteger)section animated:(BOOL)animated{
   
    if ([self numberOfItemsInSection:section]) {

        UICollectionViewCell *firstCell = [self dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:[NSIndexPath indexPathForItem:0 inSection:section]];
        CGPoint origin = firstCell.frame.origin;
        DMFlowLayout *layout = (DMFlowLayout *)self.collectionViewLayout;
        origin.y = origin.y - (layout.sectionInset.top + layout.sectionInset.bottom+[layout headerReferenceSize].height );
        [self scrollRectToVisible:CGRectMake(origin.x, origin.y, self.bounds.size.width, self.bounds.size.height) animated:animated];
    }
}

@end
