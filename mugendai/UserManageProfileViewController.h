//
//  UserManageProfileViewController.h
//  mugendai
//
//  Created by MINDNINE on 3/26/56 BE.
//  Copyright (c) 2556 mugendai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UserManageProfileViewController : UIViewController <UITableViewDataSource,UITableViewDelegate>

@property (strong, nonatomic) IBOutlet UIImageView *profileImage;
@property (strong, nonatomic) IBOutlet UILabel *userName;
@property (strong, nonatomic) IBOutlet UILabel *placeName;
@property (strong, nonatomic) IBOutlet UILabel *weeklyNumber;
@property (strong, nonatomic) IBOutlet UILabel *monthlyNumber;
@property (strong, nonatomic) IBOutlet UILabel *totalNumber;
@property (strong, nonatomic) IBOutlet UILabel *lblCredit;
@property (strong, nonatomic) IBOutlet UILabel *lblJoin;
@property (strong, nonatomic) IBOutlet UITextView *userBio;
@property (strong, nonatomic) IBOutlet UIButton *creditButton;
@property (strong, nonatomic) IBOutlet UIButton *historyButton;
@end
