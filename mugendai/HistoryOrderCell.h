//
//  HistoryOrderCell.h
//  pos
//
//  Created by MINDNINE on 3/26/56 BE.
//  Copyright (c) 2556 mugendai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HistoryOrderCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UIImageView *imageOrder;
@property (strong, nonatomic) IBOutlet UILabel *orderName;
@property (strong, nonatomic) IBOutlet UILabel *orderNumber;
@property (strong, nonatomic) IBOutlet UIButton *clearButton;
@end
