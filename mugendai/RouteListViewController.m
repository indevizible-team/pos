//
//  RouteListViewController.m
//  mugendai
//
//  Created by MINDNINE on 3/26/56 BE.
//  Copyright (c) 2556 mugendai. All rights reserved.
//

#import "RouteListViewController.h"
#import "RouteCell.h"
@interface RouteListViewController ()

@end

@implementation RouteListViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 10;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return  1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 55.0;
}

-(UITableViewCell *)tableView:(UITableView *)acell cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    RouteCell *cell = [acell dequeueReusableCellWithIdentifier:@"RouteCell" forIndexPath:indexPath];
    
    return cell;
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

- (IBAction)doneAction:(id)sender {
    [self dismissViewControllerAnimated:NO completion:nil];
}
@end
