//
//  DMNumPad.h
//  mugendai
//
//  Created by indevizible on 3/5/13.
//  Copyright (c) 2013 mugendai. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol DMNumPadDelegate;
@protocol DMNumPadDataSource;

@interface DMNumPad : UIViewController
@property (nonatomic, strong) id<DMNumPadDelegate,DMNumPadDataSource> delegate;
@property (nonatomic, strong) NSString *value;

@end

@protocol DMNumPadDelegate <NSObject>

-(void)numpadDidChangeValue:(DMNumPad *)vc value:(NSString *)value;
-(void)numpadDidClose:(DMNumPad *)vc;

@end

@protocol DMNumPadDataSource <NSObject>

-(NSString *)numpadTargetString:(DMNumPad*)vc;

@end