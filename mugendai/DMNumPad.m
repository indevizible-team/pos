//
//  DMNumPad.m
//  mugendai
//
//  Created by indevizible on 3/5/13.
//  Copyright (c) 2013 mugendai. All rights reserved.
//

#import "DMNumPad.h"

@interface DMNumPad ()

- (IBAction)numpadTap:(UIButton *)sender;
@end

@implementation DMNumPad

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    if ([self.delegate respondsToSelector:@selector(numpadTargetString:)]) {
        self.value = [self.delegate numpadTargetString:self];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)numpadTap:(UIButton *)sender {
    if (self.value == nil) {
        if (![sender.titleLabel.text isEqualToString:@"0"]) {
            self.value = sender.titleLabel.text;
        }
    }else{
        self.value = [NSString stringWithFormat:@"%@%@",self.value,sender.titleLabel.text];
    }
    if ([self.delegate respondsToSelector:@selector(numpadDidChangeValue:value:)]) {
        [self.delegate numpadDidChangeValue:self value:self.value];
    }
}
- (IBAction)clearBtn:(id)sender {
    self.value = nil;
    if ([self.delegate respondsToSelector:@selector(numpadDidChangeValue:value:)]) {
        [self.delegate numpadDidChangeValue:self value:self.value];
    }
}
- (IBAction)closeBtn:(id)sender {
    if ([self.delegate respondsToSelector:@selector(numpadDidClose:)]) {
        [self.delegate numpadDidClose:self];
    }
}


@end
