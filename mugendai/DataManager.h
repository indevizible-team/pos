//
//  DataManager.h
//
//  Created by Nattawut Singhchai on 12/7/12 .
//
#define CONFIG_PLIST @"infoconfig.plist"
#import <Foundation/Foundation.h>
#import "FileManager.h"
#import "PLIST.h"
//#import "User.h"
#import "DataController.h"
#import "SVProgressHUD.h"
//#import "LoginNavController.h"
#import "AFNetworking.h"
@interface DataManager : NSObject
@property(retain,nonatomic) NSDictionary *config;
@property(retain,nonatomic) NSString *ajaxPath;
@property(retain,nonatomic) AFHTTPClient* client;

#pragma mark - String Manager
+(NSString*)THB:(NSNumber*)amount;

#pragma mark Calendar
+(NSString*)getDateNowString;
+(NSString*)getDateFromDateString:(NSString*)date;

#pragma mark Back Controller
+(NSString *)getFeatureDir:(NSString*)feature;
+(NSString *)getLibraryDir;
+(CGFloat)txtRectCalc:(UITextView*)textField;
+(NSInteger)textHeight:(NSString*)text fromWidth:(NSInteger)width andFont:(UIFont*)font;
+(NSMutableDictionary *)JSONToMutableDictionary:(id)JSON;
+(AFHTTPClient *)client;
+(NSMutableDictionary *)user;

#pragma mark - font
+(UIFont *)fontSize:(NSString *)size;

#pragma mark - parameter
+(NSMutableDictionary *)param;

#pragma mark - storyboard
+(UIStoryboard *)getStoryboard;
+(UIViewController *)controllerByID:(NSString *)name;

#pragma mark - image convertion
+(NSString *)base64FromUIImage:(UIImage *)image;

#pragma mark - arrayToDictionaryArray converter
+(NSDictionary *)arrayToDictionaryArray:(NSArray *)array;

#pragma mark - screen size
+(CGSize)getScreenSize;
+(float)getHeightFromRatio:(float)ratio;
+(CGSize)getSizeFromRatio:(float)ratio;

+(BOOL)isIpad;
+(NSString *)currentHour;
+(NSString *)getHourWithDate:(NSDate *)date;
+(NSString *)currentDate;
+(NSString *)getDateWithDate:(NSDate *)date;
+(NSString *)currentMonth;
+(NSString *)getMonthWithDate:(NSDate *)date;
+(NSString *)currentYear;
+(NSString *)getYearWithDate:(NSDate *)date;
+(NSCalendar *)gregorianCalendar;
@end