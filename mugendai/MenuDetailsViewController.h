//
//  MenuDetailsViewController.h
//  mugendai
//
//  Created by MINDNINE on 3/4/56 BE.
//  Copyright (c) 2556 mugendai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Product.h"
#import "Order.h"
@protocol SendOrderDelegate,SendOrderDataSource;
@interface MenuDetailsViewController : UIViewController
@property (strong, nonatomic) id<SendOrderDataSource,SendOrderDelegate> delegate;
@end

@protocol SendOrderDataSource <NSObject>

-(Product *)orderProduct:(MenuDetailsViewController *)vc;
-(NSMutableArray *)orderListProduct:(MenuDetailsViewController *)vc;
@end

@protocol SendOrderDelegate <NSObject>

-(void)orderProduct:(MenuDetailsViewController *)vc saveOrder:(Order *)order;

@end