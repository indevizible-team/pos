//
//  SetMapCell.h
//  pos
//
//  Created by MINDNINE on 3/26/56 BE.
//  Copyright (c) 2556 mugendai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
@interface SetMapCell : UITableViewCell

@property (strong, nonatomic) IBOutlet MKMapView *map;

@end
