//
//  FoodSelectViewController.h
//  mugendai
//
//  Created by indevizible on 3/4/13.
//  Copyright (c) 2013 mugendai. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol FoodSelectDelegate;
@protocol FoodSelectDataSource;

@interface FoodSelectViewController : UITableViewController
@property (nonatomic,strong) NSArray *dataSource;
@property (nonatomic, assign) id<FoodSelectDataSource,FoodSelectDelegate> delegate;
@end
@protocol FoodSelectDelegate <NSObject>

-(void)foodSelect:(FoodSelectViewController *)vc atSection:(NSUInteger)section;

@end

@protocol FoodSelectDataSource <NSObject>

-(NSArray *)sectionDataFor:(FoodSelectViewController *)vc;

@end