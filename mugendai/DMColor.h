//
//  DMColor.h
//  POS
//
//  Created by Trash on 5/8/13.
//  Copyright (c) 2013 mugendai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DMColor : NSObject
@property (nonatomic,strong) UIColor *mainColor,*secondBright,*thirdBright,*secondDark,*thirdDark;
+(DMColor *)colorFromUIColor:(UIColor *)color;
+(DMColor *)colorFromHue:(float)hue;
-(NSDictionary *)dictionaryColorWithGraphType:(NSString *)graphType data:(NSArray *)data;
@end
