//
//  TestGraphViewController.m
//  POS
//
//  Created by Trash on 5/7/13.
//  Copyright (c) 2013 mugendai. All rights reserved.
//

#import "TestGraphViewController.h"
#import "DMColor.h"
#import "GraphDatasource.h"
@interface TestGraphViewController (){
    id localNotificationObserver;
    BOOL enableRotateRefresh;
    NSMutableArray *ratioChartData;
    BOOL deviceIsReady;
    NSMutableArray *fileList;
}

@end

@implementation TestGraphViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    fileList = [NSMutableArray new];
    if (!self.graphType) {
        self.graphType = kGraphTypeBar;
    }
    deviceIsReady = NO;
    self.view.frame = [UIScreen mainScreen].bounds;
    [self regisToNotificationCenter];

}
-(void)regisToNotificationCenter{
//    myObserver =
//    [[NSNotificationCenter defaultCenter]
//     addObserverForName:CDVPluginHandleOpenURLNotification
//     object:nil
//     queue:[NSOperationQueue mainQueue]
//     usingBlock:^(NSNotification *note) {
//         NSURL *url = note.object;
//         NSDictionary *query = [url dictionaryForQueryString];
//         NSLog(@"query : %@",query);
//         if (query[@"delete"]) {
//             [[NSFileManager defaultManager] removeItemAtPath:query[@"delete"] error: nil];
//         }else if (query[@"event"]){
//             NSLog(@"query : %@\n%@",query,self.webView);
//             if ([query[@"event"] isEqual:@"deviceReady"] && !deviceIsReady) {
//                 deviceIsReady = YES;
//                 [self deviceReady];
//             }
//         }
//         if (query[@"test"]) {
//             NSLog(@"%@",query[@"test"]);
//         }
//         
//     }];
    
    localNotificationObserver = [[NSNotificationCenter defaultCenter] addObserverForName:CDVLocalNotification object:nil queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification *note) {
        [self didReceiveLocalNotificationMessage:note.object];
    }];
}

-(void)didReceiveLocalNotificationMessage:(id)message{
    if ([message isKindOfClass:[NSDictionary class]]) {
        if (message[@"delete"]) {
            [[NSFileManager defaultManager] removeItemAtPath:message[@"delete"] error: nil];
        }
        
        if (message[@"event"]) {
            if ([message[@"event"] isEqual:@"deviceReady"]) {
                deviceIsReady = YES;
                [self deviceReady];
            }
        }
    }
}
- (IBAction)sendURL:(id)sender {
    [self.commandDelegate evalJs:@"sayHello()"];
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
        NSLog(@"OK");
}
-(void)deviceReady{
    if (self.graphDataSet && deviceIsReady) {
        NSLog(@"isPieChart =  %u",_isRatioChart);
        NSDate *date = [NSDate date];
        NSString *file = [PLIST write:(_isRatioChart)?[self generateRatioData]:_graphDataSet toFile:[NSString stringWithFormat:@"%@",date.description.md5]];
        [fileList addObject:file];
        NSLog(@"file -> %@ - %@",file,_graphType);
        [self.commandDelegate evalJs:[NSString stringWithFormat:@"showGraph('%@',%f,%f,'%@');",file,self.view.frame.size.width,self.view.frame.size.height-10.0f,self.graphType]];
    }
}
-(NSMutableArray *)generateRatioData{
    NSMutableArray *tmpData = [NSMutableArray new];
    for (NSDictionary *data in _graphDataSet[@"datasets"]) {
        float sum = 0;
        for (NSNumber *value in data[@"data"]) {
            sum+=[value floatValue];
        }
        [tmpData addObject:@{@"value":[NSNumber numberWithFloat:sum],@"color":data[@"color"]}];
    }
    return tmpData;
}
-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:localNotificationObserver];

    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    BOOL A = ((UIInterfaceOrientationIsLandscape(self.interfaceOrientation)) == UIInterfaceOrientationIsLandscape(toInterfaceOrientation));
    BOOL B = ((UIInterfaceOrientationIsPortrait(self.interfaceOrientation)) == UIInterfaceOrientationIsPortrait(toInterfaceOrientation));
    enableRotateRefresh = !(A||B);
}
-(void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation{
    if (enableRotateRefresh) {
        [self deviceReady];
    }
    
}
-(void)reloadGraph{
    [self deviceReady];
}


@end
