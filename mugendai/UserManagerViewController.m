//
//  UserManagerViewController.m
//  mugendai
//
//  Created by MINDNINE on 3/26/56 BE.
//  Copyright (c) 2556 mugendai. All rights reserved.
//

#import "UserManagerViewController.h"
#import "UserManagerCell.h"
#import "UserManageProfileViewController.h"
#import "ProfileSettingViewController.h"
@interface UserManagerViewController ()

@end

@implementation UserManagerViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)addMember:(id)sender {
    UIStoryboard *storyboard;
    NSDictionary *storyboardList = [[NSBundle mainBundle] infoDictionary];
    storyboard = [UIStoryboard storyboardWithName:[storyboardList valueForKey:@"UIMainStoryboardFile"] bundle:nil];
    
    ProfileSettingViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"ProfileSetting"];
    [self presentViewController:vc animated:NO completion:nil];
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return 6;
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)cv cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UserManagerCell *cell;
    cell = [cv dequeueReusableCellWithReuseIdentifier:@"UserManagerCell" forIndexPath:indexPath];
    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    UIStoryboard *storyboard;
    NSDictionary *storyboardList = [[NSBundle mainBundle] infoDictionary];
    storyboard = [UIStoryboard storyboardWithName:[storyboardList valueForKey:@"UIMainStoryboardFile"] bundle:nil];
    
    UserManageProfileViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"UserProfile"];
    [self presentViewController:vc animated:NO completion:nil];
}
- (IBAction)backButton:(id)sender {
    [self dismissViewControllerAnimated:NO completion:nil];
}

@end
