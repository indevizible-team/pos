//
//  UserManagerViewController.h
//  mugendai
//
//  Created by MINDNINE on 3/26/56 BE.
//  Copyright (c) 2556 mugendai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UserManagerViewController : UIViewController <UICollectionViewDataSource,UICollectionViewDelegate>

@property (strong, nonatomic) IBOutlet UICollectionView *collectionUserManager;
@end
