//
//  SetImageThumbCell.h
//  pos
//
//  Created by MINDNINE on 3/26/56 BE.
//  Copyright (c) 2556 mugendai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SetImageThumbCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UIButton *imageThumb;
@property (strong, nonatomic) IBOutlet UIButton *btnChooseFile;
@end
