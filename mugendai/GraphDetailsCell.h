//
//  GraphDetailsCell.h
//  POS
//
//  Created by Trash on 5/9/13.
//  Copyright (c) 2013 mugendai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GraphDetailsCell : UICollectionViewCell
@property (strong, nonatomic) IBOutlet UIView *colorTag;
@property (strong, nonatomic) IBOutlet UILabel *textLabel;

@end
