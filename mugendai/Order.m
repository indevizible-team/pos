//
//  Order.m
//  mugendai
//
//  Created by indevizible on 3/5/13.
//  Copyright (c) 2013 mugendai. All rights reserved.
//

#import "Order.h"

@implementation Order
+(Order *)orderFromProduct:(Product *)prd quantity:(int)quantity{
    Order *_order = [Order new];
    _order.productid = prd.id;
    _order.product = prd;
    _order.quantity = quantity;
    _order.remark = @"";
    return  _order;
}
-(NSDictionary *)getOrderParm{
    if (self) {
        return @{@"productid": self.productid,
                @"quantity":[NSNumber numberWithInt:self.quantity],
                @"remark":self.remark
                };
    }
    return @{};
}
@end
