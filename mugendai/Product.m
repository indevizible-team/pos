//
//  Product.m
//  mugendai
//
//  Created by indevizible on 3/5/13.
//  Copyright (c) 2013 mugendai. All rights reserved.
//

#import "Product.h"

@implementation Product
+(Product *)productWithData:(NSDictionary *)data{
    NSNumberFormatter * f = [[NSNumberFormatter alloc] init];
    [f setNumberStyle:NSNumberFormatterDecimalStyle];
    Product *_product = [Product new];
    _product.id = [f numberFromString:data[@"productid"]];
    _product.name = data[@"title"];
    _product.description = data[@"description"];
    _product.regularPrice = [[f numberFromString:data[@"regularprice"]] floatValue];
    _product.salePrice = [[f numberFromString:data[@"saleprice"]] floatValue];
    _product.imageURL = [NSURL URLWithString:data[@"pic"]];
    _product.catid = data[@"catid"];
    return  _product;
}
@end
