//
//  DMColor.m
//  POS
//
//  Created by Trash on 5/8/13.
//  Copyright (c) 2013 mugendai. All rights reserved.
//

#import "DMColor.h"
#import "DMGraphDefine.h"
@implementation DMColor
+(DMColor *)colorFromUIColor:(UIColor *)color{
    DMColor *col = [DMColor new];
    col.mainColor = color;
    float hue;
    [color getHue:&hue saturation:nil brightness:nil alpha:nil];
    col.secondBright = [UIColor colorWithHue:hue saturation:.75f brightness:1.0f alpha:1];
    col.thirdBright = [UIColor colorWithHue:hue saturation:.55f brightness:1.0f alpha:1];
    col.secondDark  = [UIColor colorWithHue:hue saturation:.75f brightness:.75f alpha:1];
    col.thirdDark   = [UIColor colorWithHue:hue saturation:1.0f brightness:.65f alpha:1];
    
    return col;
    
}
+(DMColor *)colorFromHue:(float)hue{
    DMColor *col = [DMColor new];
    col.mainColor = [UIColor colorWithHue:hue saturation:1 brightness:1 alpha:1];
    col.secondBright = [UIColor colorWithHue:hue saturation:.75f brightness:1.0f alpha:1];
    col.thirdBright = [UIColor colorWithHue:hue saturation:.55f brightness:1.0f alpha:1];
    col.secondDark  = [UIColor colorWithHue:hue saturation:.75f brightness:.75f alpha:1];
    col.thirdDark   = [UIColor colorWithHue:hue saturation:1.0f brightness:.65f alpha:1];
    
    return col;
}
-(NSDictionary *)dictionaryColorWithGraphType:(NSString *)graphType data:(NSArray *)data{
    NSMutableDictionary *dataSet = [NSMutableDictionary new];
    dataSet[@"color"]       =   [self colorStringFromUIColor:self.secondBright alpha:1.0];
    dataSet[@"fillColor"]   =   [self colorStringFromUIColor:self.thirdBright alpha:([graphType isEqual:kGraphTypeBar]?1.0:0.1) ];
    dataSet[@"strokeColor"] =   [self colorStringFromUIColor:self.secondDark alpha:1.0f];
    dataSet[@"pointColor"]  =   [self colorStringFromUIColor:self.mainColor alpha:1.0f];
    dataSet[@"pointStrokeColor"]    =   [self colorStringFromUIColor:self.thirdDark alpha:1.0];
    dataSet[@"data"]        =   data;
    return dataSet;
}

-(NSString *)colorStringFromUIColor:(UIColor *)color alpha:(float)alpha{
    float r,g,b;
    [color getRed:&r green:&g blue:&b alpha:nil];
    return [NSString stringWithFormat:@"rgba(%.0f,%.0f,%.0f,%.2f)",r*255.0f,g*255.0f,b*255.0f,alpha];
}

@end
