//
//  GraphViewerViewController.m
//  POS
//
//  Created by Trash on 5/8/13.
//  Copyright (c) 2013 mugendai. All rights reserved.
//

#import "GraphViewerViewController.h"
#import "TestGraphViewController.h"
#import "DataManager.h"
#import "Product.h"
#import "DMColor.h"
#import "GraphDetailsCell.h"
#import "GraphDatasource.h"
typedef enum {
    MainGraphTypeOverview,
    MainGraphTypeDetails
}mainGraphType;
@interface GraphViewerViewController ()<UICollectionViewDataSource,UICollectionViewDelegate,UIPopoverControllerDelegate>
{
    GraphDatasource *graph;
    TestGraphViewController *graphView;
    NSArray *graphTypeSet;
    NSMutableDictionary *product;
    NSMutableArray *labels;
    NSMutableArray *lableSets;
    NSMutableArray *graphData;
    UIPopoverController *popoverController;
    NSMutableDictionary *stat;
    UIDatePicker *datePicker;
    id statData;
}
@property (strong, nonatomic) IBOutlet UITextField *textField;
@property (strong, nonatomic) IBOutlet UISegmentedControl *graphTypeSelecter;
@property (strong, nonatomic) IBOutlet UISegmentedControl *graphRatioSelector;
@property (strong, nonatomic) IBOutlet UICollectionView *collectionView;
@end

@implementation GraphViewerViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
//    _reportType = DMReportTypeMonthly;
    _reportType = DMReportTypeDaily;
    stat = [PLIST readFeature:@"stat"];
    graphTypeSet = @[kGraphTypeBar,kGraphTypeLine,kGraphTypeRadar];
    _year = [DataManager currentYear];
    _month = [DataManager currentMonth];
    _date = [DataManager currentDate];
    product = [PLIST readFeature:@"product"];
   
    [self setBackgroundImage:[UIImage imageNamed:@"bg.png"]];
    [self collectGraphData];
}

-(void)setBackgroundImage:(UIImage *)image{
    UIImageView *backgroundImage = [[UIImageView alloc] initWithImage:image];
    [self.view addSubview:backgroundImage];
    [self.view sendSubviewToBack:backgroundImage];
}

-(void)updateLabelSets{
    lableSets = [NSMutableArray new];
    // Time labels
    labels = [NSMutableArray new];
    for (int i=0; i<24; i++) {
        [labels addObject:[NSString stringWithFormat:@"%02i:00",i]];
    }
    [lableSets addObject:labels];
    
    // Date labels
    NSDateFormatter *dateDf = [[NSDateFormatter alloc] init];
    [dateDf setCalendar:[DataManager gregorianCalendar]];
    [dateDf setDateFormat:@"yyyy-MM-dd"];
    NSDate *thatDay = [dateDf dateFromString:[NSString stringWithFormat:@"%@-%@-01",_year,_month]];
    NSRange days = [[DataManager gregorianCalendar] rangeOfUnit:NSDayCalendarUnit
                                                         inUnit:NSMonthCalendarUnit
                                                        forDate:thatDay];
    NSMutableArray *dateLabels = [NSMutableArray new];
    for (int i=0; i<days.length; i++) {
        [dateLabels addObject:[NSString stringWithFormat:@"%i",i+1]];
    }
    [lableSets addObject:dateLabels];
    // Month labels
    NSMutableArray *monthLabel = [NSMutableArray new];
    NSDateFormatter *df = [NSDateFormatter new];
    for (int i=0; i<12; i++) {
        [monthLabel addObject:[df monthSymbols][i]];
    }
    [lableSets addObject:monthLabel];
    
    NSMutableArray *yearLabel = [NSMutableArray new];
    // Year labels
    for (NSString *key in stat) {
        if (![key isEqual:@"data"]) {
            [yearLabel addObject:key];
        }
    }
    [lableSets addObject:yearLabel];

}

-(void)collectGraphData{
    [self updateLabelSets];
    graphData = [NSMutableArray new];
    statData = nil;
    if ((_reportType == DMReportTypeDaily) && stat[_year][_month][_date]) {
        statData = stat[_year][_month][_date];
    }else if ((_reportType == DMReportTypeMonthly) && stat[_year][_month]){
        statData = stat[_year][_month];
    }else if ((_reportType == DMReportTypeYearly) && stat[_year]){
        statData = stat[_year];
    }else if ((_reportType == DMReportTypeAll) && stat){
        statData = stat;
    }

    if (statData) {
        NSMutableDictionary *data = statData[@"data"];
        for (NSString *eachData in data) {
            NSArray *dataComp = [eachData componentsSeparatedByString:@"-"];
            NSString *catid = dataComp[0];
            NSString *productid = dataComp[1];
            Product *eachProduct = [Product productWithData:product[catid][@"product"][productid]];
            NSMutableArray *tmpData = [NSMutableArray new];
            for (int i = 0; i < [lableSets[_reportType] count]; i++) {
                NSString *reportKey;
                if (_reportType == DMReportTypeAll) {
                    reportKey = lableSets[_reportType][i];
                }else{
                    reportKey = [NSString stringWithFormat:(_reportType == DMReportTypeDaily)?@"%02i":@"%i",i+1];
                }
                [tmpData addObject:(statData[reportKey][@"data"][eachData])?statData[reportKey][@"data"][eachData]:@0];
            }
            NSLog(@"tmpData : %@",tmpData);
            [graphData addObject:[@{@"product": eachProduct,@"data":tmpData,@"hide":@NO} mutableCopy]];
        }
    }else{
        NSLog(@"No data");
        [SVProgressHUD showErrorWithStatus:@"NO Data"];
    }
    [self changeGraphType:_graphRatioSelector];
     [self.collectionView reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)backBtn:(id)sender {
      [self.navigationController popViewControllerAnimated:YES];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([segue.identifier isEqualToString:@"ContainerGraph"]) {
        graphView = [segue destinationViewController];
    }
}
- (IBAction)changeBarType:(UISegmentedControl *)sender {
    [_graphRatioSelector setSelectedSegmentIndex:MainGraphTypeDetails];

        graphView.graphType = graphTypeSet[sender.selectedSegmentIndex];
    graphView.isRatioChart = NO;
        [self reloadGraphData];
        [self.collectionView reloadData];
    
  
}

- (IBAction)changeGraphType:(UISegmentedControl *)sender {
    if (sender.selectedSegmentIndex == MainGraphTypeOverview) {
        graphView.isRatioChart = YES;
        graphView.graphType = @"Pie";
    }else{
        graphView.graphType = graphTypeSet[_graphTypeSelecter.selectedSegmentIndex];
        graphView.isRatioChart = NO;
    }
    [self reloadGraphData];
}


-(void)reloadGraphData{
    if (_graphRatioSelector.selectedSegmentIndex == MainGraphTypeDetails) {
        graph = [GraphDatasource dataSourceWithLabels:lableSets[_reportType] graphType:graphTypeSet[_graphTypeSelecter.selectedSegmentIndex]];
    }else{
        graph = [GraphDatasource dataSourceWithLabels:lableSets[_reportType] graphType:@"Pie"];
    }
        int i=0;
    for (NSDictionary *data in graphData) {
        if (![data[@"hide"] boolValue]) {
            [graph addDataSet:data[@"data"] withHUEColor:(1.0/graphData.count)*i];
        }
        i++;
    }
    graphView.graphDataSet = graph.dataSets;
    [graphView reloadGraph];
   
}
- (IBAction)showAll:(id)sender {
    for (NSMutableDictionary *_tmp in graphData)_tmp[@"hide"]=@NO;
    [self reloadGraphData];
    [self.collectionView reloadData];
}
- (IBAction)hideAll:(id)sender {
    for (NSMutableDictionary *_tmp in graphData) _tmp[@"hide"]=@YES;
    [self reloadGraphData];
    [self.collectionView reloadData];
}

- (void)changeDate:(UIDatePicker *)sender {
    NSDate *date = sender.date;
    _year = [DataManager getYearWithDate:date];
    _month = [DataManager getMonthWithDate:date];
    _date = [DataManager getDateWithDate:date];
    [self collectGraphData];
}
- (IBAction)selectDate:(UIButton *)sender {
    UIViewController* popoverContent = [[UIViewController alloc] init]; //ViewController
    UIView *popoverView = [[UIView alloc] init];   //view
    popoverView.backgroundColor = [UIColor blackColor];
    if (!datePicker) {
        datePicker=[[UIDatePicker alloc]init];//Date picker
        datePicker.datePickerMode = UIDatePickerModeDate;
        [datePicker addTarget:self action:@selector(changeDate:) forControlEvents:UIControlEventValueChanged];
    }
    
    [popoverView addSubview:datePicker];
    popoverContent.view = popoverView;
    popoverController = [[UIPopoverController alloc] initWithContentViewController:popoverContent];
    popoverController.delegate=self;

    [popoverController setPopoverContentSize:datePicker.frame.size animated:YES];
    [popoverController presentPopoverFromRect:sender.frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
}

#pragma mark - UICollectionView
#pragma mark  UICollectionViewDataSource
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return graphData.count;
}
-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    Product *eachProduct = graphData[indexPath.row][@"product"];
    NSString *productKey = [NSString stringWithFormat:@"%@-%@",eachProduct.catid,eachProduct.id];
    GraphDetailsCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
    [cell.colorTag setBackgroundColor:([graphData[indexPath.row][@"hide"] boolValue])?[UIColor grayColor]:[DMColor colorFromHue:(1.0/graphData.count)*indexPath.row].secondBright];
    [cell.textLabel setText:[NSString stringWithFormat:@"(%@) %@",statData[@"data"][productKey],eachProduct.name]];
    [cell.textLabel setTextColor:([graphData[indexPath.row][@"hide"] boolValue])?[UIColor blackColor]:[UIColor whiteColor]];
    return cell;
}
#pragma mark UICollectionViewDelegate
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    graphData[indexPath.row][@"hide"] = [NSNumber numberWithBool:![graphData[indexPath.row][@"hide"] boolValue]];
    [self reloadGraphData];
    [self.collectionView reloadItemsAtIndexPaths:@[indexPath]];
}
- (IBAction)search:(id)sender {
    [graphView.commandDelegate evalJs:_textField.text];
}
- (IBAction)test2:(id)sender {

}
@end
