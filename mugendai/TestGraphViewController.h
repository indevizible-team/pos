//
//  TestGraphViewController.h
//  POS
//
//  Created by Trash on 5/7/13.
//  Copyright (c) 2013 mugendai. All rights reserved.
//
#import "DMGraphDefine.h"
#import <Cordova/CDVViewController.h>
#import "DMURL.h"
#import "PLIST.h"
@interface TestGraphViewController : CDVViewController
@property (nonatomic,strong) NSString *graphType;
@property (nonatomic,strong) NSDictionary *graphDataSet;
@property (nonatomic,assign) BOOL isRatioChart;
-(void)reloadGraph;
@end
