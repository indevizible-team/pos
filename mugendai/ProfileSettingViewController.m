//
//  ProfileSettingViewController.m
//  mugendai
//
//  Created by MINDNINE on 3/26/56 BE.
//  Copyright (c) 2556 mugendai. All rights reserved.
//

#import "ProfileSettingViewController.h"
#import "SetDatailsCell.h"
#import "SetImageThumbCell.h"
#import "SetMapCell.h"
#import "SetOtherCell.h"
@interface ProfileSettingViewController ()

@end

@implementation ProfileSettingViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{

    [super viewDidLoad];
    defaultColor = UIColorFromRGB(6896663);
    images = [NSArray arrayWithObjects:
                       [UIImage imageNamed:@"iconName.png"],
                       [UIImage imageNamed:@"iconBday.png.png"],
                       [UIImage imageNamed:@"iconGender.png"],
                       [UIImage imageNamed:@"iconPlace.png"],
                       [UIImage imageNamed:@"iconMail.png"],
                       [UIImage imageNamed:@"iconPhone.png"], nil];
    title = [NSArray arrayWithObjects:@"Name",@"Birthday",@"Gender",@"Route",@"Email",@"Telephone", nil];
    gender = [NSArray arrayWithObjects:@"Male",@"Female",nil];
    
    placeholder = [NSArray arrayWithObjects:@"Name",@"",@"",@"Route",@"example@infostant.com",@"0xx-xxx-xxxx", nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 3;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    switch (section) {
        case 0:
            return 20;
            break;
        case 1:
            return 10;
            break;
        case 2:
            return 10;
            break;
        default:
            return 10;
            break;
    }
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    switch (section) {
        case 0:
            return 7;
            break;
        case 1:
            return 1;
            break;
        case 2:
            return 1;
            break;
        default:
            return 0;
            break;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section==0)
    {
        if (indexPath.row==0) {
            return 130.0;
        }
        else{
            return 50.0;
        }
    }else if (indexPath.section==1 && indexPath.row==0)
    {
        return 360.0;
    }
    else
    {
        return 50.0;
    }
}

-(UITableViewCell *)tableView:(UITableView *)atableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section==0)
    {
        if (indexPath.row==0) {
            SetImageThumbCell *cell = [atableView dequeueReusableCellWithIdentifier:@"ImageThumbCell" forIndexPath:indexPath];
            return cell;
        }
        else{
            SetDatailsCell *cell = [atableView dequeueReusableCellWithIdentifier:@"DetailsCell" forIndexPath:indexPath];
            [cell.icon setImage:[images objectAtIndex:indexPath.row-1]];
            [cell.title setText:[title objectAtIndex:indexPath.row-1]];
            [cell.txtField setPlaceholder:[placeholder objectAtIndex:indexPath.row-1]];
            if (indexPath.row==2 || indexPath.row==3)
            {
//                [cell.txtField setEnabled:NO];
//                [cell.txtField setUserInteractionEnabled:NO];
//                cell.textLabel.frame = CGRectMake(165, 0, 285, 50);
//                cell.textLabel.text = @"DD/MM/YYYY";
                [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
                [cell setSelectionStyle:UITableViewCellSelectionStyleGray];
                UIView *bgColorView = [[UIView alloc] init];
                [bgColorView setBackgroundColor:defaultColor];
                [cell setSelectedBackgroundView:bgColorView];
            }
            return cell;
        }
    }
    else if (indexPath.section==1)
    {
        SetMapCell *cell = [atableView dequeueReusableCellWithIdentifier:@"MapCell" forIndexPath:indexPath];
        return cell;
    }
    else if (indexPath.section==2)
    {
         SetOtherCell *cell = [atableView dequeueReusableCellWithIdentifier:@"OtherCell" forIndexPath:indexPath];
        [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
        [cell setSelectionStyle:UITableViewCellSelectionStyleGray];
        UIView *bgColorView = [[UIView alloc] init];
        [bgColorView setBackgroundColor:defaultColor];
        [cell setSelectedBackgroundView:bgColorView]; 
        return cell;
    }
    else
    {
        SetOtherCell *cell = [atableView dequeueReusableCellWithIdentifier:@"OtherCell" forIndexPath:indexPath];
        return cell;
    }
}

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}
-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return gender.count;
}

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    return [gender objectAtIndex:row];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section==0 && indexPath.row==2) {
        [_dayPicker setHidden:NO];
        [_tools setHidden:NO];
    
    }
    if (indexPath.section==0 && indexPath.row==3) {
        [_genderPicker setHidden:NO];
        [_tools setHidden:NO];
    }
    
}

- (IBAction)dateSelect:(UIDatePicker *)sender {
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat: @"dd/MM/YYYY"];
    
    NSDate *date = sender.date;
    [self.settingTable cellForRowAtIndexPath:[NSIndexPath indexPathForRow:2 inSection:0]].textLabel.text = [dateFormat stringFromDate:date];

}

- (IBAction)tollsDone:(id)sender {
    [_dayPicker setHidden:YES];
    [_genderPicker setHidden:YES];
    [_tools setHidden:YES];
}

- (IBAction)closeView:(id)sender {
    [self dismissViewControllerAnimated:NO completion:nil];
}

- (IBAction)doneToSave:(id)sender {
    [self dismissViewControllerAnimated:NO completion:nil];
}

@end
