//
//  HomeBarViewController.m
//  POS
//
//  Created by Trash on 5/7/13.
//  Copyright (c) 2013 mugendai. All rights reserved.
//

#import "HomeBarViewController.h"
#import "TestGraphViewController.h"
#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]
@interface HomeBarViewController ()

@end

@implementation HomeBarViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    NSLog(@"%d",0xFFFFFF);
    
//    https://search.twitter.com/search.json?q=apple
    NSURL *url = [NSURL URLWithString:@"https://search.twitter.com/search.json?q=ipad"];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    AFJSONRequestOperation *operation = [AFJSONRequestOperation
                                         JSONRequestOperationWithRequest:request
                                         success:^(NSURLRequest *request, NSHTTPURLResponse *response, id json) {
                                             NSLog(@"Tweets: %@", [json valueForKeyPath:@"results"]);
                                         } failure:nil];
    
    [operation start];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    TestGraphViewController *graphView = [segue destinationViewController];
    if ([segue.identifier isEqualToString:@"Bar"]) {
        graphView.graphType = kGraphTypeBar;
    }else if ([segue.identifier isEqualToString:@"Line"]) {
        graphView.graphType = kGraphTypeLine;
    }else{
        graphView.graphType = kGraphTypeRadar;
    }
}
- (IBAction)slide:(UISlider *)sender {
    UIColor *color = [self colorFromValue:sender.value];

    sender.minimumTrackTintColor = sender.maximumTrackTintColor =color;
    CGFloat mainHue;
    [color getHue:&mainHue saturation:nil brightness:nil alpha:nil];
    NSLog(@"main hue : %f",mainHue);
}
- (UIColor *)colorFromValue:(float)value{
    return [UIColor colorWithHue:value saturation:1.0f brightness:1.0f alpha:1.0f];
}
@end
