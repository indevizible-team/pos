//
//  GraphViewController.m
//  POS
//
//  Created by Trash on 5/7/13.
//  Copyright (c) 2013 ;. All rights reserved.
//

#import "GraphViewController.h"
#import "PLIST.h"
#import "DMURL.h"
@interface GraphViewController (){
    id myObserver;
}

@end

@implementation GraphViewController
@synthesize viewController;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}



- (void)viewDidLoad
{
    [super viewDidLoad];
    viewController = [CDVViewController new];
    viewController.view.frame = self.view.frame ;
    for (id subview in viewController.webView.subviews)
        if ([[subview class] isSubclassOfClass: [UIScrollView class]])
            ((UIScrollView *)subview).bounces = NO;
    [self.view addSubview:viewController.view];
    
    
	// Do any additional setup after loading the view.
    id notiCenter = [NSNotificationCenter defaultCenter];
   myObserver = [notiCenter addObserverForName:CDVPluginHandleOpenURLNotification
                object:nil
                queue:[NSOperationQueue mainQueue]
                usingBlock:^(NSNotification *note) {
                NSURL *url = note.object;
                NSDictionary *query = [url dictionaryForQueryString];
                if (query[@"delete"]) {
                  [[NSFileManager defaultManager] removeItemAtPath:query[@"delete"] error: nil];
                }else if (query[@"event"]){
                    if ([query[@"event"] isEqual:@"deviceReady"]) {
                        [self deviceReady];
                    }
                }
                    if (query[@"test"]) {
                        NSLog(@"%@",query[@"test"]);
                    }

    }];
    
}
-(void)deviceReady{

    NSDictionary *lineChartData =  @{
                                     @"labels" : @[@"January",@"February",@"March",@"April",@"May",@"June",@"July"],
                                     @"datasets" : @[
                                             @{
                                                 @"fillColor" : @"rgba(220,220,220,0.5)",
                                                 @"strokeColor" : @"rgba(220,220,220,1)",
                                                 @"pointColor" : @"rgba(220,220,220,1)",
                                                 @"pointStrokeColor" : @"#fff",
                                                 @"data" : @[@65,@59,@90,@81,@56,@55,@40]
                                                 },
                                             @{
                                                 @"fillColor" : @"rgba(151,187,205,0.5)",
                                                 @"strokeColor" : @"rgba(151,187,205,1)",
                                                 @"pointColor" : @"rgba(151,187,205,1)",
                                                 @"pointStrokeColor" : @"#fff",
                                                 @"data" : @[@28,@48,@40,@19,@96,@27,@100]
                                                 }
                                             ]
                                     
                                     };
    
    
    NSString *file = [PLIST write:lineChartData toFile:@"tmpFile.JSON"];
    [viewController.commandDelegate evalJs:[NSString stringWithFormat:@"showGraph('%@',%f,%f,'Radar');",file,viewController.view.frame.size.width,viewController.view.frame.size.height-10.0f]];
}
-(void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration{
    [viewController.view removeFromSuperview];
}
-(void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation{
    viewController.view.frame = self.view.frame;
    [self.view addSubview:viewController.view];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewDidDisappear:(BOOL)animated{
    [[NSNotificationCenter defaultCenter] removeObserver:myObserver];
    myObserver = nil;
    [super viewDidDisappear:animated];
    viewController = nil;
    
}
- (IBAction)test:(id)sender {
    [viewController.commandDelegate evalJs:@"sayHello();" ];
    
}


@end
