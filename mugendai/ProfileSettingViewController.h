//
//  ProfileSettingViewController.h
//  mugendai
//
//  Created by MINDNINE on 3/26/56 BE.
//  Copyright (c) 2556 mugendai. All rights reserved.
//

#import <UIKit/UIKit.h>
#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

@interface ProfileSettingViewController : UIViewController <UITableViewDataSource,UITableViewDelegate,UIPickerViewDataSource,UIPickerViewDelegate,UITextFieldDelegate>
{
    NSArray *images;
    NSArray *title;
    NSArray *placeholder;
    NSArray *gender;
    UIColor *defaultColor;
}
@property (strong, nonatomic) IBOutlet UITableView *settingTable;

@property (strong, nonatomic) IBOutlet UIPickerView *genderPicker;
@property (strong, nonatomic) IBOutlet UIDatePicker *dayPicker;
@property (strong, nonatomic) IBOutlet UIToolbar *tools;
@end
