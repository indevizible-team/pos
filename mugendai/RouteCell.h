//
//  RouteCell.h
//  pos
//
//  Created by MINDNINE on 3/26/56 BE.
//  Copyright (c) 2556 mugendai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RouteCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UIButton *tagColor;
@property (strong, nonatomic) IBOutlet UITextField *areaZone;
@property (strong, nonatomic) IBOutlet UITextField *placeZone;
@end
