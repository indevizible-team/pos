//
//  MenuCollectionCell.h
//  mugendai
//
//  Created by MINDNINE on 3/4/56 BE.
//  Copyright (c) 2556 mugendai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MenuCollectionCell : UICollectionViewCell

@property (strong, nonatomic) IBOutlet UIImageView *menuImage;
@end
