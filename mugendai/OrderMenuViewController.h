//
//  OrderMenuViewController.h
//  mugendai
//
//  Created by MINDNINE on 3/4/56 BE.
//  Copyright (c) 2556 mugendai. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import "UICollectionView+DMScroll.h"
#import "FoodSelectViewController.h"

@interface OrderMenuViewController : UIViewController <UITableViewDataSource,UITableViewDelegate,UICollectionViewDataSource,UICollectionViewDelegate,UITextFieldDelegate,UIPopoverControllerDelegate,FoodSelectDataSource,FoodSelectDelegate,UICollectionViewDelegateFlowLayout>{
    NSArray *sectionList;
}

@property (strong, nonatomic) IBOutlet UICollectionView *collectionMenu;
@property (strong, nonatomic) IBOutlet UITableView *tableorder;
@property (strong, nonatomic) IBOutlet UIButton *btnSelectType;
@property (strong, nonatomic) IBOutlet UIButton *btnCheckOrder;
@property (strong, nonatomic) IBOutlet UIButton *btnClearOrder;
@property (strong, nonatomic) IBOutlet UITextField *couponid;
@property (strong, nonatomic) IBOutlet UITextField *memberid;
@property (strong, nonatomic) IBOutlet UITextField *recievemoney;
@property (strong, nonatomic) IBOutlet UITextField *changemoney;
@property (strong, nonatomic) IBOutlet UILabel *lblTaxid;
@property (strong, nonatomic) IBOutlet UILabel *lblTaxabb;
@property (strong, nonatomic) IBOutlet UILabel *lblVat;
@property (strong, nonatomic) IBOutlet UILabel *lblInvoice;
@property (strong, nonatomic) IBOutlet UILabel *lblDate;
@property (strong, nonatomic) IBOutlet UILabel *totalLabel;
@property (strong, nonatomic) IBOutlet UIImageView *outerFrame;
@property (strong, nonatomic) IBOutlet UIButton *updateBtn;

@property (strong, nonatomic) IBOutlet UITextField *allTextField;

@end
