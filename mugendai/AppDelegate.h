//
//  AppDelegate.h
//  mugendai
//
//  Created by MINDNINE on 3/4/56 BE.
//  Copyright (c) 2556 mugendai. All rights reserved.
//

#import <UIKit/UIKit.h>
#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

@interface AppDelegate : UIResponder <UIApplicationDelegate>
{
    UIColor *defaultColor;
}


@property (strong, nonatomic) UIWindow *window;

@end
