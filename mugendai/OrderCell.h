//
//  OrderCell.h
//  mugendai
//
//  Created by MINDNINE on 3/4/56 BE.
//  Copyright (c) 2556 mugendai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OrderCell : UITableViewCell

//@property (weak, nonatomic) IBOutlet UIButton *btnCancel;
@property (strong, nonatomic) IBOutlet UIButton *btnCancel;
@property (strong, nonatomic) IBOutlet UIImageView *orderImg;
@property (strong, nonatomic) IBOutlet UILabel *orderName;
@property (strong, nonatomic) IBOutlet UILabel *orderSet;
@end
