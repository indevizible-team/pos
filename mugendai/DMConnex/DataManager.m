//
//  DataManager.m
//  Mugendai
//
//  Created by Nattawut Singhchai on 12/7/12 .
//  Copyright (c) 2012 Lekk . All rights reserved.
//

#import "DataManager.h"
//#import "NSData+Base64.h"
//#import "User.h"
#import "NSStringAdditions.h"

@implementation DataManager
@synthesize config,client;
-(id)init{
	self = [super init];
    if (self) {
		config =[PLIST readBundle:@"config.plist"];
		client= [DataManager client];
    }
    return self;
}
-(void)updateFeature:(NSString *)feature{
	NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
	[parameters setObject:[[config valueForKey:@"infofid"] valueForKey:feature] forKey:@"infofid"];
	[parameters setObject:@"1" forKey:@"json"];
	NSMutableURLRequest *myRequest = [client requestWithMethod:@"GET" path:@"getNewAllFeaturedata" parameters:parameters];
	AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:myRequest success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
		if ([JSON count]) {
			[PLIST writeFeature:feature withData:JSON];
		}
	} failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
		NSLog(@"%@\n%@",error,JSON);
	}];
	[operation start];
}




+(NSString *)getFeatureDir:(NSString*)feature{
	NSDictionary* p =[PLIST readBundle:@"config.plist"];;
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES);
	NSString *path = [[paths objectAtIndex:0] stringByAppendingPathComponent:[[p valueForKey:@"imageFolderName"] stringByAppendingPathComponent:  [NSString stringWithFormat:@"%@",[[p valueForKey:@"imageFolder"] valueForKey:feature]]]];
	return path;
	
}
+(NSString *)getLibraryDir{
    //NSArray *paths = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES);
    //NSString *documentsDirectory = [paths objectAtIndex:0];
    //documentsDirectory = [documentsDirectory stringByAppendingPathComponent:@"InfostantPrivates"];
//	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES);
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	return [[paths objectAtIndex:0] stringByAppendingPathComponent:@"InfostantPrivates"];
}
+(CGFloat)txtRectCalc:(UITextView*)textField{
	CGRect frame = textField.frame;
    UIEdgeInsets inset = textField.contentInset;
    frame.size.height = textField.contentSize.height + inset.top + inset.bottom;
	textField.frame = frame;
	
	return textField.frame.size.height;
}
+(NSInteger)textHeight:(NSString*)text fromWidth:(NSInteger)width andFont:(UIFont*)font{
	UITextView *textView = [UITextView new];
	textView.text = text;
	textView.font = font;
	textView.frame = CGRectMake(0, 0, width, 2);
	[self txtRectCalc:textView];
	NSInteger theHeight = textView.frame.size.height;
//	textView = nil;
	return theHeight;
}

#pragma mark Calendar
+(NSString *)getDateNowString{
	NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
	NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:@"gregorian"];
	[dateFormatter setDateFormat: @"yyyy-MM-dd HH:mm:ss"];
	[dateFormatter setCalendar:gregorian];
	[dateFormatter setTimeZone: [NSTimeZone timeZoneWithName:@"GMT+7"]];
	NSString *sqlDate = [dateFormatter stringFromDate: [NSDate date]];
	return sqlDate;
}
+(NSString *)getDateFromDateString:(NSString *)date{
	NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
	NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:@"gregorian"];
	[dateFormatter setDateFormat: @"yyyy-MM-dd HH:mm:ss"];
	[dateFormatter setCalendar:gregorian];
	[dateFormatter setTimeZone: [NSTimeZone timeZoneWithName:@"GMT+7"]];
	NSDate *setDate = [dateFormatter dateFromString:date];
	[dateFormatter setDateFormat:@"HH:mm"];
	NSString *sqlDate = [dateFormatter stringFromDate: setDate];
	return sqlDate;
}
+(NSCalendar *)gregorianCalendar{
    return [[NSCalendar alloc] initWithCalendarIdentifier:@"gregorian"];
}
+(NSString *)getDateWithFormat:(NSString *)format{
	return [self getDateWithFormat:format withDate:nil];
}
+(NSString *)getDateWithFormat:(NSString *)format withDate:(NSDate *)date{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
	NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:@"gregorian"];
	[dateFormatter setDateFormat: format];
	[dateFormatter setCalendar:gregorian];
	[dateFormatter setTimeZone: [NSTimeZone timeZoneWithName:@"GMT+7"]];
	NSString *sqlDate = [dateFormatter stringFromDate: (date)?date:[NSDate date]];
	return sqlDate;
}

+(NSString *)currentHour{
    return [self getDateWithFormat:@"H"];
}
+(NSString *)getHourWithDate:(NSDate *)date{
    return [self getDateWithFormat:@"H" withDate:date];
}
+(NSString *)currentDate{
  return  [self getDateWithFormat:@"d"];
}
+(NSString *)getDateWithDate:(NSDate *)date{
    return [self getDateWithFormat:@"d" withDate:date];
}
+(NSString *)currentMonth{
    return  [self getDateWithFormat:@"M"];
}
+(NSString *)getMonthWithDate:(NSDate *)date{
    return [self getDateWithFormat:@"M" withDate:date];
}
+(NSString *)currentYear{
    return  [self getDateWithFormat:@"yyyy"];
}
+(NSString *)getYearWithDate:(NSDate *)date{
    return [self getDateWithFormat:@"yyyy" withDate:date];
}

+(NSString *)THB:(NSNumber *)amount{
//	NSNumberFormatter * formatter = [[NSNumberFormatter alloc] init];
//	formatter.numberStyle = NSNumberFormatterCurrencyStyle;
//    formatter.currencyCode = @"THB";
//	return [formatter stringFromNumber:amount];
	return [NSString stringWithFormat:@"%.2f THB",[amount floatValue]];
}
+(NSMutableDictionary *)JSONToMutableDictionary:(id)JSON{
    if (![JSON count]) {
        return [@{@"key": @[]} mutableCopy];
    }
    NSMutableDictionary *mutableCopy = (NSMutableDictionary *)CFBridgingRelease(CFPropertyListCreateDeepCopy(kCFAllocatorDefault, (CFDictionaryRef)JSON, kCFPropertyListMutableContainers));
    return mutableCopy;
}
+(NSMutableArray *)ConvertToMutableArray:(NSArray *)arr{
    NSMutableArray *mutableCopy = (NSMutableArray *)CFBridgingRelease(CFPropertyListCreateDeepCopy(kCFAllocatorDefault, (CFArrayRef)arr, kCFPropertyListMutableContainers));
    return mutableCopy;
}
+(AFHTTPClient *)client{
    NSDictionary *mainInfo = [[NSBundle mainBundle] infoDictionary];
    return [AFHTTPClient clientWithBaseURL:[NSURL URLWithString:[mainInfo valueForKey:@"ajaxDomain"]]];
}
+(NSMutableDictionary *)user{
    NSMutableDictionary *_user = [PLIST readFeature:@"user"];
    if (_user.count > 1) {
        return _user;
    }
    return nil;
}

#pragma mark - font
+(UIFont *)fontSize:(NSString *)size{
    NSString *device;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        device = @"ipad";
    }else{
        device = @"iphone";
    }
    NSDictionary *fontList =  [[[NSBundle mainBundle] infoDictionary] valueForKey:@"font"];
if([[fontList valueForKey:device] valueForKey:size]==nil){
    return nil;
}
return [UIFont fontWithName:[[[fontList valueForKey:device] valueForKey:size] valueForKey:@"font"] size:[[[[fontList valueForKey:device] valueForKey:size] valueForKey:@"size"] floatValue]];
}
+(BOOL)isIpad{
    return (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad);
}
+(NSMutableDictionary *)param{
    NSDictionary *mainInfo = [[NSBundle mainBundle] infoDictionary];
    NSNumber *infoid = [mainInfo valueForKey:@"infoid"];
    return [NSMutableDictionary dictionaryWithDictionary:@{@"json":@1,@"infoid":infoid}];
}
+(UIStoryboard *)getStoryboard{
    UIStoryboard *storyboard;
    NSDictionary *storyboardList = [[NSBundle mainBundle] infoDictionary];
    storyboard = [UIStoryboard storyboardWithName:[storyboardList valueForKey:@"UIMainStoryboardFile"] bundle:nil];
    return storyboard;
}
+(UIViewController *)controllerByID:(NSString *)name{
    return [[self getStoryboard] instantiateViewControllerWithIdentifier:name];
}

#pragma mark - image convertion
+(NSString *)base64FromUIImage:(UIImage *)image{
    NSData *image64 = UIImageJPEGRepresentation(image, 1.0);
    return [NSString base64StringFromData:image64 length:[image64 length]];
}

#pragma mark - arrayToDictionaryArray converter
+(NSDictionary *)arrayToDictionaryArray:(NSArray *)array{
    NSMutableDictionary *tmpDict = [NSMutableDictionary new];
    int count = 0;
    for (id data in array) {
        [tmpDict setValue:data forKey:[NSString stringWithFormat:@"%i",count++]];
    }
    return tmpDict;
}

#pragma mark - screen size
+(CGSize)getScreenSize{
    return  [[UIScreen mainScreen] bounds].size;
    
}
+(float)getHeightFromRatio:(float)ratio{
    return [self getScreenSize].width/ratio;
}
+(CGSize)getSizeFromRatio:(float)ratio{
    return CGSizeMake([self getScreenSize].width, [self getHeightFromRatio:ratio]);
}


@end

