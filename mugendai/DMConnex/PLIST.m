//
//  PLIST.m
//  Mugendai
//
//  Created by Nattawut Singhchai on 12/7/12 .
//  Copyright (c) 2012 Lekk . All rights reserved.
//

#import "PLIST.h"
#import "DataManager.h"
@implementation PLIST
+(NSDictionary *)readPlist:(NSString *)filename{
	
	NSString *target = [[PLIST getLibraryDir] stringByAppendingPathComponent:filename];
	[self createFileIfNotExists:target];
		NSMutableDictionary *tmpPlist = [[NSMutableDictionary alloc]  initWithContentsOfFile:target];
	return tmpPlist;
	
}
+(NSMutableDictionary*)writePlist:(NSDictionary *)dic toFile:(NSString *)filename{
    [FileManager createFullDirectory:[PLIST getLibraryDir]];
	NSString *target = [NSString stringWithFormat:@"%@",[[PLIST getLibraryDir] stringByAppendingPathComponent:filename]];
	NSLog(@"writeable : %i",[[NSFileManager defaultManager] isWritableFileAtPath:target]);
	[dic writeToFile:target atomically:YES];
	
	return [self readPlist:filename];
}
+(NSString *)write:(id)data toFile:(NSString *)filename{
    NSString *target = [NSString stringWithFormat:@"%@",[[PLIST getTempDir] stringByAppendingPathComponent:filename]];
    NSLog(@"writeable2 : %i",[[NSFileManager defaultManager] isWritableFileAtPath:target]);
    
    
    // Creating JSON data and JSON String
    NSDictionary *params = data;
    // convert object to data
    NSError *error;
    NSData* jsonData = [NSJSONSerialization dataWithJSONObject:params options:0 error:&error];
    // you can convert this "jsonData" to "NSString" like below
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    [jsonString writeToFile:target atomically:YES encoding:NSUTF8StringEncoding error:&error];
    return target;

}
+(NSMutableDictionary *)readConfig{
	NSMutableDictionary* plistDict = [[NSMutableDictionary alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"infoconfig" ofType:@"plist"]];
	return plistDict;
}
+(NSMutableDictionary*)readBundle:(NSString*)filename{
	NSMutableDictionary* plistDict = [[NSMutableDictionary alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:filename ofType:@"plist"]];
	return plistDict;
}

+(NSString *)getTempDir{
	return NSTemporaryDirectory();
}
+(NSString *)getDocumentDir{
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	return [[paths objectAtIndex:0] stringByAppendingPathComponent:@"InfostantPrivates"];
}
+(NSString *)getLibraryDir{
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES);
	return [[paths objectAtIndex:0] stringByAppendingPathComponent:@"InfostantPrivates"];
}

+(NSMutableDictionary *)readFeature:(NSString *)feature{
	return [self readPlist:[NSString stringWithFormat:@"info%@.plist",feature]];
}
+(NSMutableDictionary *)writeFeature:(NSString *)feature withData:(NSDictionary *)dic{
	return [self writePlist:dic toFile:[NSString stringWithFormat:@"info%@.plist",feature]];
}
+(void)emptyFeature:(NSString *)feature{
	[self writePlist:@{} toFile:[NSString stringWithFormat:@"info%@.plist",feature]];
}
+(void)createFileIfNotExists:(NSString*)path{
	 NSMutableDictionary *data;
	NSFileManager *filemgr = [NSFileManager defaultManager];
	if ([filemgr fileExistsAtPath: path ]){
		data = [[NSMutableDictionary alloc] initWithContentsOfFile: path];
	}else{
		data = [[NSMutableDictionary alloc] init];
	}
	[data writeToFile: path atomically:YES];
}
@end