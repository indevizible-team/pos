//
//  FileManager.h
//  Mugendai
//
//  Created by Nattawut Singhchai on 12/7/12 .
//  Copyright (c) 2012 Lekk . All rights reserved.
//

#import <Foundation/Foundation.h>


@interface FileManager : NSObject

+(BOOL)createDirectory:(NSString*)dirname;
//+(NSString*)downloadFileFromURL:(NSString*)url toDirectory:(NSString*)dir fileName:(NSString*)filename;
//+(void)downloadFileFromURL:(NSString*)url toDocument:(NSString*)dir fileName:(NSString*)filename;
//+(void)uploadImageAndDataToURL:(NSString*)url image:(UIImage*)image	data:(NSDictionary*)data fileKey:(NSString *)key success:(void (^)(id messsage))success fail:(void (^)(NSError* error)) fail;
//+(void)postDataTo:(NSString *)url data:(NSDictionary *)data success:(void (^)(id message))success fail:(void (^)(NSError *error))fail;
//+(UIImage*)imageFromFeature:(NSString*)feature filename:(NSString*)path;
//+(UIImage*)imageFromRelateURL:(NSString*)url;
//+(void)httpPost:(NSString *)path data:(NSDictionary*)data;
//+(void)jsonPost:(NSString *)path data:(NSDictionary*)data;
//+(void)downloadMultiFile:(NSArray *)url toProgress:(UIProgressView*)pg success:(void (^)(void))success fail:(void (^)(void))fail;
+(BOOL)removeDirectoryFromUrl:(NSURL*)url;
+(BOOL)removeDirectory:(NSString*)dirname;
+(BOOL)createFullDirectory:(NSString*)dirname;
//+(void)saveJSON:(id)JSON ToPlist:(NSString*)feature;
@end
