//
//  PLIST.h
//  Mugendai
//
//  Created by Nattawut Singhchai on 12/7/12 .
//  Copyright (c) 2012 Lekk . All rights reserved.
//
#import <Foundation/Foundation.h>

@interface PLIST : NSObject
+(NSMutableDictionary*)readPlist:(NSString*)filename;
+(NSMutableDictionary*)writePlist:(NSDictionary*)dic toFile:(NSString*)filename;
+(NSMutableDictionary*)readBundle:(NSString*)filename;
+(NSString *)getLibraryDir;
+(NSString *)getDocumentDir;
+(NSString *)write:(id)data toFile:(NSString *)filename;

+(NSMutableDictionary*)readFeature:(NSString*)feature;
+(NSMutableDictionary*)writeFeature:(NSString*)feature withData:(NSDictionary *)dic;
+(void)emptyFeature:(NSString*)feature;
+(void)createFileIfNotExists:(NSString*)path;
@end
