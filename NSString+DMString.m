//
//  NSString+DMString.m
//  POS
//
//  Created by Trash on 5/8/13.
//  Copyright (c) 2013 mugendai. All rights reserved.
//
#import <CommonCrypto/CommonDigest.h>
#import "NSString+DMString.h"
#define CC_MD5_DIGEST_LENGTH 16 
@implementation NSString (DMString)
- (NSString *) md5
{
    const char *cStr = [self UTF8String];
    unsigned char digest[16];
    CC_MD5( cStr, strlen(cStr), digest ); // This is the md5 call
    
    NSMutableString *output = [NSMutableString stringWithCapacity:CC_MD5_DIGEST_LENGTH * 2];
    
    for(int i = 0; i < CC_MD5_DIGEST_LENGTH; i++)
        [output appendFormat:@"%02x", digest[i]];
    
    return  output;
    
}
@end
